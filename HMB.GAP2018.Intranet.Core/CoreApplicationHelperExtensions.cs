﻿using HMB.GAP2018.Intranet.Core.Announcements;
using HMB.GAP2018.Intranet.Core.ModelValidation;
using HMB.GAP2018.Intranet.Core.TimesSheets;
using Microsoft.Extensions.DependencyInjection;

namespace HMB.GAP2018.Intranet.Core
{
    public static class CoreApplicationHelperExtensions
    {
        public static IServiceCollection AddCoreApplicationServices(this IServiceCollection services) =>
            services
                .AddScoped<IModelValidationService, ModelValidationService>()
                .AddScoped<IAnnouncementService, AnnouncementService>()
                .AddScoped<ITimeSheetService, TimeSheetService>();
    }
}
