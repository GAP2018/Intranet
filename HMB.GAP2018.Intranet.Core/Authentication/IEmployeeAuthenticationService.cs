﻿using HMB.GAP2018.Intranet.Core.Employees;

namespace HMB.GAP2018.Intranet.Core.Authentication
{
    public interface IEmployeeAuthenticationService
    {
        Employee GetLoggedInEmployee();
    }
}
