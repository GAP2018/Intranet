﻿using System;
using System.Collections.Generic;

namespace HMB.GAP2018.Intranet.Core.TimesSheets
{
    public interface ITimeSheetService
    {
        TimeSheet GetTimeSheet(DateTime dateInWeek);
        bool SubmitTimesheet(TimeSheet timeSheet);
        Dictionary<string, IEnumerable<string>> Validate(TimeSheet timeSheet);
    }
}