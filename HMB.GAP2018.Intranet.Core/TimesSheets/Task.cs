﻿using System.ComponentModel.DataAnnotations;

namespace HMB.GAP2018.Intranet.Core.TimesSheets
{
    public class Task
    {
        public int Id { get; set; }

        [Required, StringLength(300)]
        public string Name { get; set; }

        public bool IsNoteRequired { get; set; }
    }
}