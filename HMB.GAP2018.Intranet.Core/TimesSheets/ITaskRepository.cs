﻿using System.Linq;

namespace HMB.GAP2018.Intranet.Core.TimesSheets
{
    public interface ITaskRepository
    {
        IQueryable<Task> GetAll();
    }
}