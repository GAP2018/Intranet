﻿using System;
using HMB.GAP2018.Intranet.Core.Employees;

namespace HMB.GAP2018.Intranet.Core.TimesSheets
{
    public interface ITimeSheetRepository
    {
        TimeSheet GetByEmployeeAndStartOfWeek(Employee employee, DateTime startOfWeek);
        void Submit(TimeSheet sheet);
    }
}
