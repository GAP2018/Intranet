﻿using System;
using System.Collections.Generic;
using HMB.GAP2018.Intranet.Core.Employees;

namespace HMB.GAP2018.Intranet.Core.TimesSheets
{
    public class TimeSheet
    {
        public Employee Employee { get; set; }
        public DateTime MondayOfWeek { get; set; }
        public IEnumerable<TimeEntry> Entries { get; set; }
    }
}
