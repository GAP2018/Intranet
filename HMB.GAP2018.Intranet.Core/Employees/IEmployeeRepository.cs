﻿namespace HMB.GAP2018.Intranet.Core.Employees
{
    public interface IEmployeeRepository
    {
        Employee GetEmployeeByEmail(string email);
    }
}
