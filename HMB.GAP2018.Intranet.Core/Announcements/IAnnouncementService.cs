﻿using System.Collections.Generic;

namespace HMB.GAP2018.Intranet.Core.Announcements
{
    public interface IAnnouncementService
    {
        bool CreateAnnouncement(Announcement announcement);
        bool UpdateAnnouncement(Announcement announcement);
        bool RemoveAnnouncement(int id);
        IEnumerable<Announcement> GetActiveAnnouncements();
        IEnumerable<Announcement> GetAllAnnouncements();
    }
}
