﻿import { Component } from "@angular/core";
import { AnnouncementService } from "../home/announcement.service";
import { Subscription } from "rxjs/Subscription";
import { Announcement } from "../home/announcement.model";


@Component({
    selector: 'announcementlist',
    templateUrl: './announcementlist.component.html'
})

export class AnnouncementListComponent {

    private _subscriptions: Subscription[] = [];
    public announcements: Announcement[] = [];

    constructor(private _announcementService: AnnouncementService) { }

    public ngOnInit() {
        const messageSubscription = this._announcementService.getAllAnnouncements()
            .subscribe(list => {
                this.announcements = list;
            });

        this._subscriptions = [messageSubscription];
    }


    public deleteAnnouncement(id: number) {
        var doDelete = confirm("Are you sure you want to delete this announcement?");
        if (doDelete) {
            this._announcementService.deleteAnnouncement(id).subscribe(() => { });
            window.location.reload();
        }
    }

}