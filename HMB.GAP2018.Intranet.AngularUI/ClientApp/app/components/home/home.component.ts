import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/subscription';
import { Converter } from 'showdown';

import { Announcement } from './announcement.model';
import { AnnouncementService } from './announcement.service';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnDestroy, OnInit {
    private _subscriptions: Subscription[] = [];
    private _converter = new Converter();

    public announcements: Announcement[] = [];

    constructor(private _announcementService: AnnouncementService) { }

    public ngOnDestroy(): void {
        this._subscriptions.forEach(s => s.unsubscribe());
    }

    public ngOnInit() {
        const messageSubscription = this._announcementService.getActiveAnnouncements()
            .subscribe(list => {
                this.announcements = list.map(a => {
                     return { ...a, body: this._converter.makeHtml(a.body) }
                });
            });

        this._subscriptions = [messageSubscription];
    }
}
