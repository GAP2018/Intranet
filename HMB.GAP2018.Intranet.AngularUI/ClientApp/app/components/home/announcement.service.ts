﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';

import { Announcement } from './announcement.model';
@Injectable()
export class AnnouncementService {
    private readonly ROOT_ENDPOINT = 'api/Announcement';

    constructor(
        private _http: Http,
        @Inject('WEBAPI_URL') private _webApiUrl: string
    ) { }

    public getActiveAnnouncements(): Observable<Announcement[]> {
        return this._http.get(`${this._webApiUrl}${this.ROOT_ENDPOINT}/active`)
            .map((response: Response) => response.json() as Announcement[]);
    }

    public getAllAnnouncements(): Observable<Announcement[]> {
        return this._http.get(`${this._webApiUrl}${this.ROOT_ENDPOINT}`)
            .map((response: Response) => response.json() as Announcement[])
            .map<Announcement[], Announcement[]>(announcements => announcements.map(this.mapAnnouncement));
    }

    public getAnnouncementById(id: number) {
        return this._http.get(`${this._webApiUrl}${this.ROOT_ENDPOINT}/${id}`)
            .map((response: Response) => response.json() as Announcement)
            .map<Announcement, Announcement>(this.mapAnnouncement);
    }

    public deleteAnnouncement(id: number) {
        return this._http.delete(`${this._webApiUrl}${this.ROOT_ENDPOINT}/${id}`)
            .map((response: Response) => response.json());

    }

    public createAnnouncement(announcement: Announcement) {
        return this._http.post(`${this._webApiUrl}${this.ROOT_ENDPOINT}`, announcement)
    }

    public updateAnnouncement(announcement: Announcement) {
        return this._http.put(`${this._webApiUrl}${this.ROOT_ENDPOINT}`, announcement)
    }

    private mapAnnouncement(announcement: Announcement): Announcement {
        return {
            ...announcement,
            startDate: moment(announcement.startDate).format("YYYY-MM-DD"),
            endDate: moment(announcement.endDate).format("YYYY-MM-DD")
        };
    }
}
