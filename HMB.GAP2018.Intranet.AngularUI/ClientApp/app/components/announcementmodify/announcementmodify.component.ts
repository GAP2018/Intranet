﻿import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs/subscription';

import { AnnouncementService } from "../home/announcement.service";
import { Announcement } from "../home/announcement.model";

@Component({
    selector: 'announcementmodify',
    templateUrl: './announcementmodify.component.html'
})
export class AnnouncementModifyComponent implements OnDestroy, OnInit {
    private subscriptions: Subscription[] = [];

    announcement?: Announcement;
    isSaving: boolean = false;

    constructor(private readonly route: ActivatedRoute,
        private readonly announcementService: AnnouncementService,
        private readonly router: Router) { }


    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            const id = params.get('id');
            if (id) {
                this.announcementService.getAnnouncementById(+id)
                    .subscribe(announcement => this.announcement = announcement);
            }
            else {
                this.announcement = {
                    isHighPriority: false,
                    title: "",
                    body: "",
                    startDate: "",
                    endDate: ""
                }
            }
        });
    }

    save() {
        if (!this.announcement) return;

        this.isSaving = true;
        if (this.announcement.id) {
            const sub = this.announcementService.updateAnnouncement(this.announcement)
                .subscribe(() => this.redirect());
            this.subscriptions = [...this.subscriptions, sub];

        }
        else {
            const sub = this.announcementService.createAnnouncement(this.announcement)
                .subscribe(() => this.redirect());
            this.subscriptions = [...this.subscriptions, sub];            
        }
        
    }

    private redirect() {
        window.alert("Announcement Saved Successfully");
        this.router.navigate(['announcementlist']);
    }
}