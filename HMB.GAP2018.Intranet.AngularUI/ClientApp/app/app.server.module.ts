import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';

import { AnnouncementService } from './components/home/announcement.service';
@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        ServerModule,
        AppModuleShared
    ],
    providers: [
        { provide: 'WEBAPI_URL', useValue: 'http://localhost:54003/' },
        AnnouncementService
    ]
})
export class AppModule {
}
