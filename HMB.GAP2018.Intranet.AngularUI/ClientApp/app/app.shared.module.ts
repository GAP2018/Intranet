import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { AnnouncementListComponent } from './components/announcementlist/announcementlist.component';
import { TimeSheetComponent } from './components/timesheet/timesheet.component';
import { AnnouncementModifyComponent } from './components/announcementmodify/announcementmodify.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        AnnouncementListComponent,
        TimeSheetComponent,
        AnnouncementModifyComponent,
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'announcementlist', component: AnnouncementListComponent, pathMatch: 'full' },
            { path: 'timesheet', component: TimeSheetComponent },
            { path: 'announcementmodify/:id', component: AnnouncementModifyComponent },
            { path: 'announcementmodify', component: AnnouncementModifyComponent },
            { path: '**', redirectTo: 'home' },
            
        ])
    ]
})
export class AppModuleShared {
}
