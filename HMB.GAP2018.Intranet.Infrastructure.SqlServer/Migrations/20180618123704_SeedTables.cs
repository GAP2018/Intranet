﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.Migrations
{
    public partial class SeedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            SeedAnnouncementsTable(migrationBuilder);
            SeedEmployeesTable(migrationBuilder);
            SeedTasksTable(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            ClearAnnouncementsTable(migrationBuilder);
            ClearEmployeesTable(migrationBuilder);
            ClearTasksTable(migrationBuilder);
        }
    }
}
