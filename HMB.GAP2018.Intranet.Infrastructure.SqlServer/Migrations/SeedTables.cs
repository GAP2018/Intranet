﻿using System;
using System.Collections.Generic;
using HMB.GAP2018.Intranet.Core.Announcements;
using HMB.GAP2018.Intranet.Core.Employees;
using HMB.GAP2018.Intranet.Core.TimesSheets;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.Migrations
{
    public partial class SeedTables
    {
        private static readonly List<Employee> _employees;
        private static readonly List<Task> _tasks;
        private static readonly List<Announcement> _announcements;

        static SeedTables()
        {
            _announcements = new List<Announcement>
            {
                new Announcement
                {
                    StartDate = new DateTime(2018, 6, 18, 0, 0, 0, DateTimeKind.Utc),
                    EndDate = new DateTime(2018, 6, 20, 0, 0, 0, DateTimeKind.Utc),
                    IsHighPriority = true,
                    Title = "Welcome to the Angular portion of the Grad Academy Project",
                    Body = @"##About
This is a markdown file. for more information about markdown please go to [this site](https://www.markdownguide.org/cheat-sheet)
",
                },
                new Announcement
                {
                    StartDate = new DateTime(2018, 6, 20, 0, 0, 0, DateTimeKind.Utc),
                    EndDate = new DateTime(2018, 6, 24, 0, 0, 0, DateTimeKind.Utc),
                    IsHighPriority = false,
                    Title = "Grad Academy Information",
                    Body = @"##Members
* Heather Barnes
* Jake Bieller
* Braden Campbell
* Matt Keister
* Katie Posey
* Adam Powell
* Sam Seelbach
* Colt Thompson
* John Tito
* Ben Wicker
* Zaren Wienclaw
",
                },
                new Announcement
                {
                    StartDate = new DateTime(2018, 6, 20, 0, 0, 0, DateTimeKind.Utc),
                    EndDate = new DateTime(2018, 6, 24, 0, 0, 0, DateTimeKind.Utc),
                    IsHighPriority = true,
                    Title = "Welcome to the Grad Academy Presentation!",
                    Body = @"##Project Breakdown
* Front end: [Angular](https://angular.io/)
* API: [ASP.NET Core 2.0 Web API](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1)
* Database: [SQLite](https://www.sqlite.org/index.html)
",
                }
            };
            _employees = new List<Employee>
            {
                new Employee { Email = "alice.jones.admin@hmbnet.com", FirstName = "Alice", LastName = "Jones" },
                new Employee { Email = "john.smith.employee@hmbnet.com", FirstName = "John", LastName = "Smith" },
                new Employee { Email = "pamela.rogers.pm@hmbnet.com", FirstName = "Pamela", LastName = "Rogers" },
                new Employee { Email = "carter.cruz.hr@hmbnet.com", FirstName = "Carter", LastName = "Cruz" }
            };

            _tasks = new List<Task>
            {
                new Task { Name = "Deployment" },
                new Task { Name = "Design" },
                new Task { Name = "Develop" },
                new Task { Name = "Marketing" },
                new Task { Name = "Meeting" },
                new Task { Name = "Other", IsNoteRequired= true },
                new Task { Name = "Out of Office" },
                new Task { Name = "Project Management" },
                new Task { Name = "Release Management" },
                new Task { Name = "Requirements" },
                new Task { Name = "Testing" }
            };
        }

        private static void SeedAnnouncementsTable(MigrationBuilder migrationBuilder)
        {
            foreach (var announcement in _announcements)
            {
                migrationBuilder.InsertData(
                    table: "Announcements",
                    columns: new[] { nameof(announcement.StartDate), nameof(announcement.EndDate), nameof(announcement.IsHighPriority), nameof(announcement.Title), nameof(announcement.Body) },
                    values: new object[] { announcement.StartDate, announcement.EndDate, announcement.IsHighPriority, announcement.Title, announcement.Body });
            }
        }

        private static void ClearAnnouncementsTable(MigrationBuilder migrationBuilder)
            => migrationBuilder.Sql("DELETE * FROM Announcements");

        private static void SeedEmployeesTable(MigrationBuilder migrationBuilder)
        {
            foreach (var employee in _employees)
            {
                migrationBuilder.InsertData(
                    table: "Employees",
                    columns: new[] { nameof(employee.Email), nameof(employee.FirstName), nameof(employee.LastName) },
                    values: new object[] { employee.Email, employee.FirstName, employee.LastName });
            }
        }

        private static void ClearEmployeesTable(MigrationBuilder migrationBuilder)
            => migrationBuilder.Sql("DELETE * FROM Employees");

        private static void SeedTasksTable(MigrationBuilder migrationBuilder)
        {
            foreach (var task in _tasks)
            {
                migrationBuilder.InsertData(
                    table: "Tasks",
                    columns: new[] { nameof(task.Name), nameof(task.IsNoteRequired) },
                    values: new object[] { task.Name, task.IsNoteRequired });
            }
        }

        private static void ClearTasksTable(MigrationBuilder migrationBuilder)
            => migrationBuilder.Sql("DELETE * FROM Tasks");
    }
}
