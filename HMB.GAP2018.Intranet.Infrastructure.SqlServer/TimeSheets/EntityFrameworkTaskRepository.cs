﻿using System.Linq;
using HMB.GAP2018.Intranet.Core.TimesSheets;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.TimeSheets
{
    public class EntityFrameworkTaskRepository : ITaskRepository
    {
        private readonly IntranetContext _context;

        public EntityFrameworkTaskRepository(IntranetContext context)
        {
            _context = context;
        }

        public IQueryable<Task> GetAll() => _context.Tasks;
    }
}
