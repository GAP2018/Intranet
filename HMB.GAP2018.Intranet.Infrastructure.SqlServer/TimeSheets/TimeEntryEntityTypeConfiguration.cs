﻿using HMB.GAP2018.Intranet.Core.Employees;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.TimeSheets
{
    internal class TimeEntryEntityTypeConfiguration : IEntityTypeConfiguration<TimeEntryEntity>
    {
        public void Configure(EntityTypeBuilder<TimeEntryEntity> builder)
        {
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Property(e => e.Note)
                .HasMaxLength(1000);
        }
    }
}
