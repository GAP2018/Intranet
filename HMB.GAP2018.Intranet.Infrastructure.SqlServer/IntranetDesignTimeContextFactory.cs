﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer
{
    public class IntranetDesignTimeDbContextFactory : IDesignTimeDbContextFactory<IntranetContext>
    {
        public IntranetContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IntranetContext>();
            optionsBuilder.UseSqlite("Data Source=Intranet.db");

            return new IntranetContext(optionsBuilder.Options);
        } 
    }
}
