﻿using HMB.GAP2018.Intranet.Core.Employees;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.Employees
{
    internal class EmployeeEntityTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Property(e => e.Email)
                .HasMaxLength(300);
            builder.Property(e => e.FirstName)
                .HasMaxLength(100);
            builder.Property(e => e.LastName)
                .HasMaxLength(100);
        }
    }
}
