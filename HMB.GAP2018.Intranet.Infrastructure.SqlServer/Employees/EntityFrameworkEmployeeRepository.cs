﻿using System;
using System.Linq;
using HMB.GAP2018.Intranet.Core.Employees;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.Employees
{
    public class EntityFrameworkEmployeeRepository : IEmployeeRepository
    {
        private readonly IntranetContext _context;

        public EntityFrameworkEmployeeRepository(IntranetContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Employee GetEmployeeByEmail(string email)
            => _context.Employees.SingleOrDefault(e => e.Email == email);
    }
}
