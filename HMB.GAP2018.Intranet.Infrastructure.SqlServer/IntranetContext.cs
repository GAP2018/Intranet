﻿using HMB.GAP2018.Intranet.Core.Announcements;
using HMB.GAP2018.Intranet.Core.Employees;
using HMB.GAP2018.Intranet.Core.TimesSheets;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.Announcements;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.Employees;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.TimeSheets;
using Microsoft.EntityFrameworkCore;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer
{
    public class IntranetContext : DbContext
    {
        // ReSharper disable once SuggestBaseTypeForParameter
        public IntranetContext(DbContextOptions<IntranetContext> context) : base(context)
        { }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TimeEntryEntity> TimeEntries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AnnouncementEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaskEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TimeEntryEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
