﻿using HMB.GAP2018.Intranet.Core.Announcements;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer.Announcements
{
    internal class AnnouncementEntityTypeConfiguration : IEntityTypeConfiguration<Announcement>
    {
        public void Configure(EntityTypeBuilder<Announcement> builder)
        {
            builder.Property(a => a.Id)
                .ValueGeneratedOnAdd();
            builder.Property(a => a.Title)
                .HasMaxLength(500);
        }
    }
}
