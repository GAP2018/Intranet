﻿using HMB.GAP2018.Intranet.Core.Announcements;
using HMB.GAP2018.Intranet.Core.Employees;
using HMB.GAP2018.Intranet.Core.TimesSheets;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.Announcements;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.Employees;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer.TimeSheets;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HMB.GAP2018.Intranet.Infrastructure.SqlServer
{
    public static class DatabaseHelperExtensions
    {
        public static IServiceCollection AddEntityFrameworkRepositories(this IServiceCollection services,
            string connection) =>
            services.AddDbContext<IntranetContext>(options => options.UseSqlite(connection))
                .AddScoped<IAnnouncementRepository, EntityFrameworkAnnouncementRepository>()
                .AddScoped<IEmployeeRepository, EntityFrameworkEmployeeRepository>()
                .AddScoped<ITaskRepository, EntityFrameworkTaskRepository>()
                .AddScoped<ITimeSheetRepository, EntityFrameworkTimeSheetRepository>();

        public static void UseAutomaticIntranetDatabaseMigrations(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<IntranetContext>().Database.Migrate();
            }
        }
    }
}
