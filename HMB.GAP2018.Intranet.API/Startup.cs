﻿using System.IO;
using HMB.GAP2018.Intranet.API.Authentication;
using HMB.GAP2018.Intranet.Core;
using HMB.GAP2018.Intranet.Core.Authentication;
using HMB.GAP2018.Intranet.Infrastructure.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Internal;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace HMB.GAP2018.Intranet.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            services
                .AddApiVersioning(o => o.AssumeDefaultVersionWhenUnspecified = true)
                .AddSwaggerGen(options =>
                    {
                        options.SwaggerDoc("v1.0", new Info { Title = "HMB Grad Academy Intranet API V1", Version = "v1.0" });
                        options.DescribeAllEnumsAsStrings();

                        var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                        var xmlDocs = Path.Combine(basePath, $"{PlatformServices.Default.Application.ApplicationName}.xml");
                        if (File.Exists(xmlDocs))
                            options.IncludeXmlComments(xmlDocs);
                    });
            
            services
                .AddTransient<ISystemClock, SystemClock>()
                .AddEntityFrameworkRepositories(Configuration.GetConnectionString("HMBGAPIntranet"))
                .AddCoreApplicationServices()
                .AddScoped<IEmployeeAuthenticationService, HttpContextEmployeeAuthenticationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseAutomaticIntranetDatabaseMigrations();
            }

            app.UseCors(builder => builder.AllowAnyHeader()
                .AllowAnyMethod()
                .AllowAnyOrigin());

            app.UseMvc();

            app.UseSwagger(options => options.RouteTemplate = "api-spec/{documentName}");
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/api-spec/v1.0", "HMB Grad Academy Intranet API V1");
            });
        }
    }
}
