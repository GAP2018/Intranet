﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HMB.GAP2018.Intranet.Core.Authentication;
using HMB.GAP2018.Intranet.Core.Employees;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace HMB.GAP2018.Intranet.API.Authentication
{
    public class HttpContextEmployeeAuthenticationService : IEmployeeAuthenticationService
    {
        // TODO - use _contextAccessor to get email address once authentication is enabled
        //private readonly IHttpContextAccessor _contextAccessor;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<HttpContextEmployeeAuthenticationService> _logger;

        public HttpContextEmployeeAuthenticationService(/*IHttpContextAccessor contextAccessor,*/ IEmployeeRepository employeeRepository, ILogger<HttpContextEmployeeAuthenticationService> logger)
        {
            // _contextAccessor = contextAccessor;
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public Employee GetLoggedInEmployee()
        {
            var email = "alice.jones.admin@hmbnet.com"; // TODO - Get from _contextAccessor
            var employee = _employeeRepository.GetEmployeeByEmail(email);

            return employee;
        }
    }
}
