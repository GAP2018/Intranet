﻿using HMB.GAP2018.Intranet.Core.Announcements;
using HMB.GAP2018.Intranet.Core.Authentication;
using HMB.GAP2018.Intranet.Core.Employees;
using HMB.GAP2018.Intranet.Core.ModelValidation;
using Microsoft.Extensions.Internal;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace HMB.GAP2018.Intranet.Tests.Announcements
{
    public class AnnouncementServiceShould
    {
        [Fact]
        public void RequireALoggedInUserWhenCreatingAnAnnouncement()
        {
            // Arrange
            var mockAuthenticationService = new Mock<IEmployeeAuthenticationService>();
            var mockAnnouncementRepository = new Mock<IAnnouncementRepository>();
            var mockValidationService = new Mock<IModelValidationService>();
            var mockSystemClock = new Mock<ISystemClock>();
            var mockLogger = new Mock<ILogger<AnnouncementService>>();

            var sut = new AnnouncementService(
                mockAnnouncementRepository.Object,
                mockAuthenticationService.Object,
                mockValidationService.Object,
                mockSystemClock.Object,
                mockLogger.Object
            );

            mockAuthenticationService
                .Setup(s => s.GetLoggedInEmployee()).Returns<Employee>(null);

            // Act
            var actual = sut.CreateAnnouncement(new Announcement());

            // Assert
            Assert.False(actual);
            mockAnnouncementRepository.Verify(r => r.Add(It.IsAny<Announcement>()), Times.Never);
        }
    }
}
