﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMB.GAP2018.Intranet.Core.Employees;

namespace HMB.GAP2018.Intranet.Infrastructure.InMemory
{
    public class InMemoryEmployeeRepository : IEmployeeRepository
    {
        private static readonly List<Employee> _employees;

        static InMemoryEmployeeRepository()
        {
            _employees = new List<Employee>
            {
                new Employee { Email = "admin@istrator.com", FirstName = "Admin", LastName = "Istrator" },
                new Employee { Email = "john.smith.iii@hmbnet.com", FirstName = "John", LastName = "Smith" }
            };
        }

        public Employee GetEmployeeByEmail(string email)
        {
            var employee = _employees.SingleOrDefault(e => e.Email.Equals(email, StringComparison.OrdinalIgnoreCase));

            return employee;
        }
    }
}
